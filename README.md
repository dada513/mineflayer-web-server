[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

# mineflayer-web-server

HTTP API to remotely control [mineflayer]() bots.

### Documentation

API doc available on [postman](https://documenter.getpostman.com/view/11314149/T1DjjzeW?version=latest)
