const express = require('express')
const path = require('path')

module.exports = (bot, config) => {
  require('./lib/bot').setBot(bot)

  const app = express()
  app.use(require('express').json())
  app.use('/api/', require('./routes/api'))

  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
  })

  app.use(require('./lib/error').errorHandler)
  const port = config.WebServer.port || 3000
  app.listen(port, () => console.log('Listening on ' + port))
}
