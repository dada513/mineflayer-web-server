class AppError {
  constructor (msg) {
    this.message = msg
  }
}
const errorHandler = (err, req, res, next) => {
  if (err instanceof AppError) {
    res.status(400)
    res.send(err.message)
    return next()
  } else {
    res.status(500)
    res.send('Unknown error.')
    return next()
  }
}

module.exports.AppError = AppError
module.exports.errorHandler = errorHandler
