const router = require('express').Router()
const { bot } = require('../lib/bot')

let isLoggedIn = false
let isSpawned = false
bot.once('login', () => {
  isLoggedIn = true
})
bot.once('spawn', () => {
  isSpawned = true
})

router.get('/', (req, res) => {
  res.json({
    isLoggedIn,
    isSpawned
  })
})

router.use(require('./chat'))
router.use(require('./setControlState'))

module.exports = router
