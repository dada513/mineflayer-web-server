const router = require('express').Router()
const { bot } = require('../lib/bot')
const { AppError } = require('../lib/error')
// const gameChat = []
bot.chatAddPattern()

router.post('/chat', (req, res) => {
  if (!req.body || !req.body.message) {
    throw new AppError('no chat message to send provided')
  }
  bot.chat(req.body.message)
  res.json(req.body)
})

module.exports = router
