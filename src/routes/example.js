/* eslint-disable */ // <- dont copy this line
// template router for other endpoints
const router = require("express").Router();
const { bot } = require("../lib/bot");
const { AppError } = require("../lib/error");

module.exports = router;
