const router = require('express').Router()
const { bot } = require('../lib/bot')
const { AppError } = require('../lib/error')
const controlstates = [
  'forward',
  'back',
  'left',
  'right',
  'jump',
  'sprint',
  'sneak'
]

router.post('/setControlState', (req, res) => {
  if (!req.body || !req.body.control || !req.body.state) {
    throw new AppError('Invalid body.')
  }
  if (typeof req.body.state !== 'boolean') {
    throw new AppError('state should be a boolean')
  }
  if (!controlstates.includes(req.body.control)) {
    throw new AppError(
      "control must be one of: ['forward', 'back', 'left', 'right', 'jump', 'sprint', 'sneak']"
    )
  }
  bot.setControlState(req.body.control, req.body.state)
  return res.json(req.body)
})

module.exports = router
