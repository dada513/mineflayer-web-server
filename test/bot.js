const mineflayer = require('mineflayer')
const webServer = require('../src/index')
console.log('Loading...')
const bot = mineflayer.createBot({
  username: 'testbot',
  plugins: [webServer],
  WebServer: { port: 3000 }
})

bot.once('spawn', () => {
  console.log('spawned')
})
